import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class MeetingService {

  constructor(private _client: HttpClient) {}

  deleteMeeting(index: number){
    return this._client.put(`${environment.apiUrl}/meetings/${index}`, {
      isAborted: true
    });
  }

  getMeetings(filter: any){
    let params = new URLSearchParams();
    params.append('page', filter.page ? filter.page : 1);
    let date = new Date();
    params.append('isAborted', 'false');
    if(filter.date){
      params.append('startedAt[after]', filter.date.toJSON());
    }else{
      params.append('startedAt[after]', date.toJSON());
    }
    if(filter.retro != null){
      params.append('retro', filter.retro);
    }
    params.append('order[startedAt]', 'asc');
    return this._client.get(`${environment.apiUrl}/meetings?${params.toString()}`);
  }

  getPassedMeetings(filter: any){
    let params = new URLSearchParams();
    params.append('page', filter.page ? filter.page : 1);
    let date = new Date();
    if(filter.date){
      params.append('startedAt[after]', filter.date.toJSON());
    }else{
      params.append('startedAt[before]', date.toJSON());
    }
    if(filter.retro != null){
      params.append('retro', filter.retro);
    }
    params.append('order[startedAt]', 'asc');
    return this._client.get(`${environment.apiUrl}/meetings?${params.toString()}`);
  }

  postMeetings(data: any){
    return this._client.post(`${environment.apiUrl}/meetings`, data);
  }

  updateMeeting(data: any, index: number){
    return this._client.put(`${environment.apiUrl}/meetings/${index}`, data);
  }

  getMeetingsForPeriode(start: string, end: string){
    let params = new URLSearchParams();
    params.append('isAborted', 'false');
    params.append('startedAt[after]', `${start}`);
    params.append('startedAt[before]', `${end}`);
    params.append('order[startedAt]', 'asc');
    return this._client.get(`${environment.apiUrl}/meetings?${params.toString()}`)
  }

  getGlobalStats(){
    return this._client.get(`${environment.apiUrl}/meetings/stats`)
  }

  getMonthStats(){
    return this._client.get(`${environment.apiUrl}/meetings/monthly`)
  }

  getRecents(){
    let date = new Date();
    let params = new URLSearchParams();
    params.append('isAborted', 'false');
    params.append('order[createdAt]', 'desc');
    params.append('startedAt[after]', date.toJSON());
    return this._client.get(`${environment.apiUrl}/meetings?${params.toString()}`);
  }

  getUpcomming(){
    let date = new Date();
    let params = new URLSearchParams();
    params.append('isAborted', 'false');
    params.append('page', '1');
    params.append('startedAt[after]', date.toJSON());
    params.append('order[startedAt]', 'asc');
    return this._client.get(`${environment.apiUrl}/meetings?${params.toString()}`);
  }
}
