import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {BehaviorSubject, Observable, throwError} from "rxjs";
import {AuthService} from "./auth.service";
import {catchError, switchMap, take, filter} from "rxjs/operators";
import {TokenService} from "./token.service";

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor{

  private isRefresh = false
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private _authService: AuthService, private _tokenService: TokenService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // @ts-ignore
    let token: string | null = this._authService.getToken();
    let tokenOnRequest: any;
    if(token){
      tokenOnRequest = req.clone({
        setHeaders: {
          'Authorization': token
        }
      });
    }else {
      tokenOnRequest = req.clone({});
    }

    // @ts-ignore
    return next.handle(tokenOnRequest).pipe(catchError((error: any) => {
      if(error.status === 401){
        return this.handle401(req, next);
      }
      return throwError(error);
    }));
  }

  handle401(request: HttpRequest<any>, next: HttpHandler){
    if(!this.isRefresh) {
      this.isRefresh = true;
      this.refreshTokenSubject.next(null);
      let refresh = this._authService.getRefreshToken();
      if (refresh) {
        return this._authService.refreshToken().pipe(
          switchMap((token: any) => {
            this.isRefresh = false;
            this._tokenService.saveToken(token.token);
            this.refreshTokenSubject.next(token.token);

            return next.handle(this.addTokenHeader(request, token.token));
          }),
          catchError((err) => {
            this.isRefresh = false;

            this._tokenService.signOut();
            return throwError(err);
          })
        );
      }
    }
    return this.refreshTokenSubject.pipe(
      filter(token => token !== null),
      take(1),
      switchMap((token) => next.handle(this.addTokenHeader(request, token)))
    );
  }

  addTokenHeader(request: HttpRequest<any>, token: string) {
    return request.clone({ headers: request.headers.set('Authorization', token) });
  }
}
