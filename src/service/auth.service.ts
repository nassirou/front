import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../environments/environment";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private _client: HttpClient, private _router: Router) {
  }

  login(data: any){
    return this._client.post(`${environment.basePath}/auth/login`, data);
  }

  loggedIn(){
    return !!localStorage.getItem('token');
  }

  getToken(): string {
    // @ts-ignore
    return localStorage.getItem('token');
  }

  getRefreshToken(): string {
    // @ts-ignore
    return localStorage.getItem('refresh_token');
  }

  refreshToken(){
    return this._client.post(`${environment.basePath}/auth/refresh`, {
      token: this.getToken(),
      refresh_token: this.getRefreshToken()
    });
  }

  getUser(){
    return localStorage.getItem('personnal');
  }

  logout(){
    localStorage.removeItem('personnal');
    localStorage.removeItem('token');
    localStorage.removeItem('refresh_token');

    this._router.navigate(['/']);
  }
}
