import {Injectable} from "@angular/core";
import {Router} from "@angular/router";

@Injectable(
  {
    providedIn: 'root'
  }
)
export class TokenService{

  constructor(private _router: Router) {
  }

  saveToken(value: string){
    localStorage.setItem('token', value);
  }

  saveRefresh(value: string){
    localStorage.setItem('refresh_token', value);
  }

  user(user: any){
    localStorage.setItem('personnal', JSON.parse(user));
  }

  signOut(): void {
    window.localStorage.clear();
    this._router.navigate(['/login']);
  }
}
