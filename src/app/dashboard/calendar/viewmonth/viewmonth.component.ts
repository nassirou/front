import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Meeting} from "../../../models/meeting";
import {MeetingService} from "../../../../service/meeting.service";

@Component({
  selector: 'app-viewmonth',
  templateUrl: './viewmonth.component.html',
  styleUrls: ['./viewmonth.component.css']
})
export class ViewmonthComponent implements OnInit, OnChanges {

  allDays: Array<any> = [];
  readonly weekDays: Array<string> = ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'];
  @Input() indexDate: any;
  @Input() currentDateDay: number = 0;
  listing: Array<Meeting> = [];
  @Output() changeMonth = new EventEmitter<string>();
  today: Date = new Date();
  @Output() addMeeting = new EventEmitter<any>();
  @Input() elementToAdd: any;

  constructor(private _meetingService: MeetingService) {}

  handleEdit(){
    this.findForDay();
  }

  stopEvent($event: any){
    $event.stopPropagation();
  }

  deleteMeeting($event: any){
    this._meetingService.deleteMeeting($event).subscribe(
      (data: any) => {
        this.listing = this.listing.filter((x: Meeting) => x.id != $event);
      },
      error => {

      }
    )
  }

  isSameDate(day: number, event: Meeting){
    let $often = new Date(event.startedAt);
    return day === $often.getDate();
  }

  fixMonthElement(){
    var today = new Date(this.indexDate);
    var lastDayOfMonth = new Date(today.getFullYear(), today.getMonth()+1, 0);
    var firstDay = new Date(today.getFullYear(), today.getMonth(), 1);
    const current = new Date(this.indexDate);
    current.setMonth(current.getMonth()-1);
    var previousMonth = new Date(current.getFullYear(), current.getMonth()+1, 0);
    var now = new Date(this.indexDate);
    var nextMonth = null;
    if (now.getMonth() == 11) {
      nextMonth = new Date(now.getFullYear() + 1, 0, 1);
    } else {
      nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
    }
    this.processFillArray(lastDayOfMonth, firstDay, previousMonth, nextMonth);
  }

  processFillArray(lastDayOfMonth: any, firstday: any, lastMonth: any, nextMonth: any){
    const fillLeft = firstday.getDay();
    const fillRight =  6 - lastDayOfMonth.getDay();
    let daysArrays: Array<any> = [];
    // @ts-ignore
    daysArrays = Array(lastDayOfMonth.getDate()).fill().map((x,i)=> {return {current: true, day: i+1}} );
    for (var i = 0; i < fillLeft; i++){
      daysArrays.unshift({current: false, position: 'pre', day: lastMonth.getDate() - i});
    }
    for (var y = 0; y < fillRight; y++){
      daysArrays.push({current: false, position: 'post', day: nextMonth.getDate() + y});
    }
    this.allDays = daysArrays;
  }

  otherMonth(value: string){
    this.changeMonth.emit(value);
  }

  addEvent(day: any){
    let alternate = new Date();
    alternate.setFullYear(this.indexDate.getFullYear());
    alternate.setMonth(this.indexDate.getMonth());
    alternate.setDate(day.day);
    if(alternate >= (new Date())){
      this.addMeeting.emit({
        data: alternate,
        hour: null
      });
    }
  }

  findForDay(){
    var $end = new Date(this.indexDate.getFullYear(), this.indexDate.getMonth()+1, 0);
    var $start = new Date(this.indexDate.getFullYear(), this.indexDate.getMonth(), 1);
    $start.setHours(0);
    $start.setMinutes(0);
    $start.setSeconds(0);
    $end.setHours(23);
    $end.setMinutes(59);
    $end.setSeconds(59);
    this._meetingService.getMeetingsForPeriode($start.toJSON(), $end.toJSON()).subscribe(
      (data: any) => {
        this.listing = data['hydra:member'];
      },
      error => {

      }
    );
  }

  ngOnInit(): void {
    this.fixMonthElement();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.listing = [];
    this.fixMonthElement();
    this.findForDay();
  }
}
