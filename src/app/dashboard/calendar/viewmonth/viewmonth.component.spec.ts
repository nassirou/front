import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmonthComponent } from './viewmonth.component';

describe('ViewmonthComponent', () => {
  let component: ViewmonthComponent;
  let fixture: ComponentFixture<ViewmonthComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewmonthComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
