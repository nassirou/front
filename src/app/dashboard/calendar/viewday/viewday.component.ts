import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {dayLabels, monthLabel, samplecolor, acceptedHours} from "../../utils/common";
import {Meeting} from "../../../models/meeting";
import {MeetingService} from "../../../../service/meeting.service";

@Component({
  selector: 'app-viewday',
  templateUrl: './viewday.component.html',
  styleUrls: ['./viewday.component.css']
})
export class ViewdayComponent implements OnInit, OnChanges {

  // @ts-ignore
  readonly dayHours: acceptedHours.type = acceptedHours;
  @Input() indexDate: any;
  @Input() currentDateDay: number = 0;
  myDate: string = '';
  listing: Array<Meeting> = [];
  // @ts-ignore
  readonly daysLabels: dayLabels.type = dayLabels;
  // @ts-ignore
  readonly monthsLabels: monthLabel.type = monthLabel;
  // @ts-ignore
  readonly colors: samplecolor.type = samplecolor;
  @Output() addMeeting = new EventEmitter<any>();
  @Input() elementToAdd: any;

  constructor(private _meetingService: MeetingService) {  }

  handleEdit(){
    this.findForDay();
  }

  deleteMeeting($event: any){
    this._meetingService.deleteMeeting($event).subscribe(
      (data: any) => {
        this.listing = this.listing.filter((x: Meeting) => x.id != $event);
      },
      error => {

      }
    )
  }

  findForDay(){
    let $start = new Date(this.indexDate.toJSON());
    $start.setHours(0);
    $start.setMinutes(0);
    $start.setSeconds(0);
    let $end = new Date(this.indexDate.toJSON());
    $end.setHours(23);
    $end.setMinutes(59);
    $end.setSeconds(59);
    this._meetingService.getMeetingsForPeriode($start.toJSON(), $end.toJSON()).subscribe(
      (data: any) => {
        this.listing = data['hydra:member'];
      },
      error => {

      }
    );
  }

  stopEvent($event: any){
    $event.stopPropagation();
  }

  isSameHour(hour: number, event: Meeting){
    let $often = new Date(event.startedAt);
    return hour === $often.getHours();
  }

  elementHeight(event: Meeting){
    let $often = new Date(event.startedAt);
    let $ends = new Date(event.endsAt);
    let height = ($ends.getHours() - $often.getHours()) * 100;
    let minutes = $ends.getMinutes() - $often.getMinutes();
    return height + ((minutes / 60) * 100);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.listing = [];
    if(!changes.indexDate.firstChange){
      this.myDate = `${this.daysLabels[this.indexDate.getDay()]} ${this.indexDate.getDate()} ${this.monthsLabels[this.indexDate.getMonth()]}`;
    }
    this.findForDay();
  }

  ngOnInit(): void {
    this.myDate = `${this.daysLabels[this.indexDate.getDay()]} ${this.indexDate.getDate()} ${this.monthsLabels[this.indexDate.getMonth()]}`;
  }

  addEvent(hour: number){
    this.indexDate.setHours(hour);
    if(this.indexDate > (new Date())){
      this.addMeeting.emit({
        data: this.indexDate,
        hour: hour
      });
    }
  }
}
