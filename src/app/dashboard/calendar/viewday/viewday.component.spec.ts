import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewdayComponent } from './viewday.component';

describe('ViewdayComponent', () => {
  let component: ViewdayComponent;
  let fixture: ComponentFixture<ViewdayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewdayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewdayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
