import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InlineeventComponent } from './inlineevent.component';

describe('InlineeventComponent', () => {
  let component: InlineeventComponent;
  let fixture: ComponentFixture<InlineeventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InlineeventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineeventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
