import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import { badges } from "../../utils/common";
import {MeetingService} from "../../../../service/meeting.service";

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.css']
})
export class MeetingComponent implements OnInit, OnChanges {

  readonly badges: typeof badges = badges;

  title: string = '';
  comment: string = '';
  retro: boolean = false;
  badge: string = '';
  started_at: string = '';
  ends_at: string = '';
  day: string = '';

  message: string = '';

  // @ts-ignore
  @Input() show : boolean;
  @Input() requestedDate: any;

  @Output() hideElement = new EventEmitter<boolean>();
  @Output() created = new EventEmitter();

  added: boolean = false;

  load: boolean = false;

  @Output() pushAdded = new EventEmitter();

  constructor(private _meetingService: MeetingService) { }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    if(changes.requestedDate && changes.requestedDate.currentValue){
      let newDate = changes.requestedDate.currentValue;
      this.started_at = newDate.startHour;
      this.ends_at = newDate.endsHour;
      this.day = newDate.data.toISOString().slice(0,10);
      this.show = true;
    }
  }

  hide(){
    this.hideElement.emit(false);
  }

  handleChooseBadge(value: string){
    this.badge = value;
  }

  handleToggleRetro(value: boolean){
    this.retro = value;
  }

  initValues(){
    this.badge = '';
    this.title = '';
    this.comment = '';
    this.retro = false;
    this.day = '';
    this.started_at = '';
    this.ends_at = '';
  }

  postMeeting(){
    if([this.title, this.comment, this.started_at, this.ends_at, this.day].every(x => x)){
      this.load = true;
      let startedDate = new Date(`${this.day} ${this.started_at}`);
      let endsDate = new Date(`${this.day} ${this.ends_at}`);

      let orderFirstInterval = new Date(`${this.day} ${this.started_at}`);
      let orderLastInterval = new Date(`${this.day} ${this.ends_at}`);

      orderFirstInterval.setHours(7);
      orderFirstInterval.setMinutes(0);
      orderFirstInterval.setSeconds(0);

      orderLastInterval.setHours(20);
      orderLastInterval.setMinutes(0);
      orderLastInterval.setSeconds(0);

      if((startedDate >= orderFirstInterval) && (endsDate <= orderLastInterval)){
        if(endsDate > startedDate){

          this.message = '';

          let data: any = {
            badge: this.badge,
            title: this.title,
            comment: this.comment,
            retro: this.retro,
            startedAt: startedDate,
            endsAt: endsDate
          };

          this._meetingService.postMeetings(data).subscribe(
            (data: any) => {
              if(data.id){
                this.initValues();
                this.show = false;
                this.load = false;
                this.added = true;
                this.pushAdded.emit(data);
                setTimeout(() => {
                  this.added = false;
                }, 5000);
              }else{
                this.load = false;
                this.message = 'Une erreur s\'est produite, essayez à nouveau';
              }
            },
            error => {
              if(error.error['hydra:description'] === "La date choisie correspond à une réunion"){
                this.message = 'La date choisie correspond à une réunion';
              }else{
                this.message = 'Une erreur s\'est produite, veillez bien remplir le formulaire';
              }
              this.load = false;
            }
          );
        }else{
          this.load = false;
          this.message = 'Veillez bien remplir le formulaire';
        }
      }else{
        this.load = false;
        this.message = 'Le réunion sont possible entre 7h:00 & 20h00';
      }
    }else{
      this.load = false;
      this.message = 'Veillez bien remplir le formulaire';
    }
  }
}
