import {NgModule} from "@angular/core";
import {CalendarComponent} from "./calendar.component";
import { ViewdayComponent } from './viewday/viewday.component';
import { ViewweekComponent } from './viewweek/viewweek.component';
import { ViewmonthComponent } from './viewmonth/viewmonth.component';
import {CommonModule} from "@angular/common";
import {MeetingComponent} from "./meeting/meeting.component";
import {FormsModule} from "@angular/forms";
import { InlineeventComponent } from './inlineevent/inlineevent.component';

@NgModule({
  declarations: [
    CalendarComponent,
    ViewdayComponent,
    ViewweekComponent,
    ViewmonthComponent,
    MeetingComponent,
    InlineeventComponent
  ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        CalendarComponent,
        MeetingComponent
    ]
})
export class CalendarModule{}
