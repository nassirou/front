import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {acceptedHours} from "../../utils/common";
import {Meeting} from "../../../models/meeting";
import {MeetingService} from "../../../../service/meeting.service";

@Component({
  selector: 'app-viewweek',
  templateUrl: './viewweek.component.html',
  styleUrls: ['./viewweek.component.css']
})
export class ViewweekComponent implements OnInit, OnChanges {

  weekDays: Array<any> = [];
  // @ts-ignore
  readonly dayHours: acceptedHours.type = acceptedHours;
  listing: Array<Meeting> = [];
  @Input() indexDate: any;
  @Input() currentDateDay: number = 0;
  @Output() addMeeting = new EventEmitter<any>();
  @Input() elementToAdd: any;

  constructor(private _meetingService: MeetingService) {}

  handleEdit(){
    this.findForDay();
  }

  deleteMeeting($event: any){
    this._meetingService.deleteMeeting($event).subscribe(
      (data: any) => {
        this.listing = this.listing.filter((x: Meeting) => x.id != $event);
      },
      error => {

      }
    )
  }

  findForDay(){
    let $firstOfWeek = this.weekDays[0];
    let $lastOfWeek = this.weekDays[this.weekDays.length - 1];
    let $start = new Date();
    $start.setDate($firstOfWeek.date);
    $start.setMonth($firstOfWeek.month - 1);
    $start.setFullYear(this.indexDate.getFullYear());
    $start.setHours(0);
    $start.setMinutes(0);
    $start.setSeconds(0);

    let $end = new Date(this.indexDate.toJSON());
    $end.setDate($lastOfWeek.date);
    $end.setMonth($lastOfWeek.month - 1);
    $end.setFullYear(this.indexDate.getFullYear());
    $end.setHours(23);
    $end.setMinutes(59);
    $end.setSeconds(59);

    this._meetingService.getMeetingsForPeriode($start.toJSON(), $end.toJSON()).subscribe(
      (data: any) => {
        this.listing = data['hydra:member'];
      },
      error => {

      }
    );
  }

  stopEvent($event: any){
    $event.stopPropagation();
  }

  isSameDate(hour: number, day: number, event: Meeting){
    let $often = new Date(event.startedAt);
    return day === $often.getDate() && hour === $often.getHours();
  }

  elementHeight(event: Meeting){
    let $often = new Date(event.startedAt);
    let $ends = new Date(event.endsAt);
    let height = ($ends.getHours() - $often.getHours()) * 100;
    let minutes = $ends.getMinutes() - $often.getMinutes();
    return height + ((minutes / 60) * 100);
  }

  proceedWeekDisplay(){
    var curr = new Date(this.indexDate);
    var first = curr.getDate() - curr.getDay();
    var firstday = new Date(curr.setDate(first));
    let elemArr = ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'];
    let newArr = [];
    let i = 0;
    do{
      newArr.push({
        date: firstday.getDate(),
        day: elemArr[i],
        month: firstday.getMonth() + 1
      });
      firstday.setDate(firstday.getDate() + 1);
      i++;
    }while(i < 7);
    this.weekDays = newArr;
  }

  ngOnInit(): void {
    this.proceedWeekDisplay();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.listing = [];
    this.proceedWeekDisplay();
    this.findForDay();
  }

  createMeeting(day: any, hour: number){
    let alternate = new Date();
    alternate.setFullYear(this.indexDate.getFullYear());
    alternate.setMonth(day.month - 1);
    alternate.setDate(day.date);
    alternate.setHours(hour);
    if(alternate > (new Date())){
      this.addMeeting.emit({
        data: alternate,
        hour: hour
      });
    }
  }
}
