import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewweekComponent } from './viewweek.component';

describe('ViewweekComponent', () => {
  let component: ViewweekComponent;
  let fixture: ComponentFixture<ViewweekComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewweekComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewweekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
