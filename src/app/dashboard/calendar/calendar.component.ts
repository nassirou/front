import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import * as moment from "moment";
import {monthLabel} from "../utils/common";

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  currentDateNumber: number = 0;
  selectedView: string = 'week';
  indexedDate: any;
  variant: number = 0;
  // @ts-ignore
  readonly monthsLabels: monthLabel.type = monthLabel;
  showModal: boolean = false;
  taggedDate: any = null;
  newElementToAdd: any;
  @Output() created = new EventEmitter();

  constructor(){
    this.currentDateNumber = (new Date()).getDate();
    this.indexedDate = new Date();
  }

  handleNewElement($event: any){
    this.newElementToAdd = $event;
  }

  mappedToday(){
    this.currentDateNumber = (new Date()).getDate();
    this.indexedDate = new Date();
    this.variant = 0;
  }

  handleCreated(){
    this.created.emit();
  }

  handleChangeModalShow($event: boolean){
    this.showModal = $event;
    this.taggedDate = null;
  }

  handleNextElement(){
    if(this.selectedView === 'day'){
      this.variant++;
    }else if(this.selectedView === 'week'){
      this.variant = this.variant + 7;
    }else if(this.selectedView === 'month'){
      this.variant = this.variant + 30;
    }
    moment(this.indexedDate).locale('fr');
    this.indexedDate = new Date(moment().add(this.variant, 'd').format());
  }

  handlePreventMonthChange($event: string){
    if($event === 'pre'){
      this.handleLastElement();
    }else{
      this.handleNextElement();
    }
  }

  handleLastElement(){
    if(this.selectedView === 'day'){
      this.variant--;
    }else if(this.selectedView === 'week'){
      this.variant = this.variant - 7;
    }else if(this.selectedView === 'month'){
      this.variant = this.variant - 30;
    }
    moment(this.indexedDate).locale('fr');
    this.indexedDate = new Date(moment().add(this.variant, 'd').format());
  }

  handleChangeView(value: string){
    this.selectedView = value;
  }

  ngOnInit(): void {}

  handleRequestNewEvent($event: any){
    let startDate = '';
    let endsDate = '';
    if($event.hour != null){
      if($event.hour < 10){
        if($event.hour === 9){
          startDate = `0${$event.hour}:00`;
          endsDate = `${$event.hour + 1}:00`;
        }else{
          startDate = `0${$event.hour}:00`;
          endsDate = `0${$event.hour + 1}:00`;
        }
      }else{
        startDate = `${$event.hour}:00`;
        endsDate = `${$event.hour + 1}:00`;
      }
      if($event.hour === 23){
        endsDate = `00:00`;
      }
    }
    this.taggedDate = {
      startHour: startDate,
      endsHour: endsDate,
      data: $event.data
    };
    this.showModal = true;
  }
}
