import { Component, OnInit } from '@angular/core';
import {MeetingService} from "../../../service/meeting.service";
import {Meeting} from "../../models/meeting";

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.css']
})
export class ResumeComponent implements OnInit {

  loadListing: boolean = true;
  listings: Array<Meeting> = [];

  indexDate = new Date();

  allDays: Array<any> = [];

  globalStats: any = {
    total: 0,
    coming: 0
  };

  calendarElement: Array<Meeting> = [];
  calendarLoad: boolean = true;

  dayEventList: Array<Meeting> = [];
  dayEventListLoad: boolean = false;
  daysEventModal: boolean = false;
  daysEventLabel: string = '';

  showModal: boolean = false;

  taggedDate: any = null

  monthStats: any = {
    all: 0,
    done: 0,
    coming: 0
  }

  dayIndex: number = (new Date()).getDate();

  latestDay: number = 0;

  requestNewEvent(){
    let startDate = '';
    let endsDate = '';

    let date = new Date();

    let hour = 0;

    if(hour < 10){
      if(hour === 9){
        startDate = `0${hour}:00`;
        endsDate = `${hour + 1}:00`;
      }else{
        startDate = `0${hour}:00`;
        endsDate = `0${hour + 1}:00`;
      }
    }else{
      startDate = `${hour}:00`;
      endsDate = `${hour + 1}:00`;
    }
    if(hour === 23){
      endsDate = `00:00`;
    }

    date.setDate(this.latestDay);

    this.taggedDate = {
      startHour: startDate,
      endsHour: endsDate,
      data: date
    };

    this.daysEventModal = false;

    this.showModal = true;
  }

  constructor(private _meetingService: MeetingService) {  }

  pushNewMeeting(data: any){
    this.listings.push(data);
    this.calendarElement.push(data);
  }

  deleteMeeting($event: any){
    this._meetingService.deleteMeeting($event).subscribe(
      (data: any) => {
        this.listings = this.listings.filter((x: Meeting) => x.id != $event);
        this.calendarElement = this.calendarElement.filter((x: Meeting) => x.id != $event);
      },
      error => {

      }
    )
  }

  closeDaysEvents(){
    this.daysEventModal = false;
    this.dayEventList = [];
  }

  findDaysEvents(day: number){
    this.latestDay = day;

    this.daysEventModal = true;
    this.dayEventListLoad = true;

    this.daysEventLabel = `${day}/${this.indexDate.getMonth() + 1}/${this.indexDate.getFullYear()}`;

    let start = new Date(`${this.indexDate.getFullYear()}-${this.indexDate.getMonth() + 1}-${day}`);
    let end = new Date(`${this.indexDate.getFullYear()}-${this.indexDate.getMonth() + 1}-${day}`);

    end.setHours(23);
    end.setMinutes(59);

    this._meetingService.getMeetingsForPeriode(start.toJSON(), end.toJSON()).subscribe(
      (data: any) => {
        this.dayEventList = data['hydra:member'];
        this.dayEventListLoad = false;
      },
      error => {

      }
    )
    console.log(start);
  }

  eventOnDate(day: number){
    return this.calendarElement.some((event: Meeting) => {
      let myDate = new Date(event.startedAt);
      return myDate.getDate() === day;
    });
  }

  ngOnInit(): void {

    this._meetingService.getGlobalStats().subscribe((data: any) => {
      this.globalStats = data['hydra:member'][0];
    });

    this._meetingService.getRecents().subscribe((data: any) => {
      this.listings = data['hydra:member'];
      this.loadListing = false;
    },
    error => {

    });

    let interval = this.getMonthStartAndEnds();
    this._meetingService.getMeetingsForPeriode(interval.first.toJSON(), interval.last.toJSON()).subscribe((data: any) => {
        this.calendarElement = data['hydra:member'];
        this.fixMonthElement();
        this.calendarLoad = false;
      },
    error => {
        console.log(error);
      }
    );

    this._meetingService.getMonthStats().subscribe(
      (data: any) => {
        this.monthStats = data['hydra:member'][0];
      },
      (error: any) => {

      }
    )
  }

  getMonthStartAndEnds(){
    var today = new Date(this.indexDate);
    var lastDayOfMonth = new Date(today.getFullYear(), today.getMonth()+1, 0);
    var firstDay = new Date(today.getFullYear(), today.getMonth(), 1);
    lastDayOfMonth.setHours(23);
    lastDayOfMonth.setMinutes(59);
    return {first: firstDay, last: lastDayOfMonth};
  }

  fixMonthElement(){

    var today = new Date(this.indexDate);
    var lastDayOfMonth = new Date(today.getFullYear(), today.getMonth()+1, 0);
    var firstDay = new Date(today.getFullYear(), today.getMonth(), 1);

    const current = new Date(this.indexDate);
    current.setMonth(current.getMonth()-1);
    var previousMonth = new Date(current.getFullYear(), current.getMonth()+1, 0);

    var now = new Date(this.indexDate);
    var nextMonth = null;
    if (now.getMonth() == 11) {
      nextMonth = new Date(now.getFullYear() + 1, 0, 1);
    } else {
      nextMonth = new Date(now.getFullYear(), now.getMonth() + 1, 1);
    }

    this.processFillArray(lastDayOfMonth, firstDay, previousMonth, nextMonth);
  }

  processFillArray(lastDayOfMonth: any, firstday: any, lastMonth: any, nextMonth: any){
    const fillLeft = firstday.getDay();
    const fillRight =  6 - lastDayOfMonth.getDay();
    let daysArrays: Array<any> = [];
    // @ts-ignore
    daysArrays = Array(lastDayOfMonth.getDate()).fill().map((x,i)=> {return {current: true, day: i+1}} );
    for (var i = 0; i < fillLeft; i++){
      daysArrays.unshift({current: false, position: 'pre', day: lastMonth.getDate() - i});
    }
    for (var y = 0; y < fillRight; y++){
      daysArrays.push({current: false, position: 'post', day: nextMonth.getDate() + y});
    }
    this.allDays = daysArrays;
  }

  handleChangeModalShow($event: boolean){
    this.showModal = $event;
  }
}
