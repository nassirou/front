import { Component, OnInit } from '@angular/core';
import {MeetingService} from "../../service/meeting.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  user: any = {};

  // @ts-ignore
  showSideBar: boolean = false;

  constructor(private _meetingService: MeetingService) {
    if(localStorage.getItem('personnal')){
      // @ts-ignore
      this.user = JSON.parse(localStorage.getItem('personnal'));
    }else{

    }
  }

  toggleSideBar(){
    this.showSideBar = !this.showSideBar;
  }

  ngOnInit(): void {}

}
