import { NgModule } from "@angular/core";
import { DashboardComponent } from "./dashboard.component";
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import {CalendarModule} from "./calendar/calendar.module";
import { BookingComponent } from './booking/booking.component';
import { ResumeComponent } from './resume/resume.component';
import {RouterModule} from "@angular/router";
import { EventComponent } from './event/event.component';
import {NgxSkeletonLoaderModule} from "ngx-skeleton-loader";
import {CommonModule} from "@angular/common";
import { ArchiveComponent } from './archive/archive.component';
import { EmptyComponent } from './empty/empty.component';
import {FormsModule} from "@angular/forms";

@NgModule({
    declarations: [
        DashboardComponent,
        HeaderComponent,
        SidebarComponent,
        BookingComponent,
        ResumeComponent,
        EventComponent,
        ArchiveComponent,
        EmptyComponent
    ],
    imports: [
        CalendarModule,
        RouterModule,
        NgxSkeletonLoaderModule,
        CommonModule,
        FormsModule
    ],
    exports: [
        DashboardComponent
    ]
})
export class DashboardModule{  }
