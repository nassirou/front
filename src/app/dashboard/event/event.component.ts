import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Meeting} from "../../models/meeting";
import * as moment from "moment";
import {badges} from "../utils/common";
import {MeetingService} from "../../../service/meeting.service";
import {AuthService} from "../../../service/auth.service";

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  readonly badges: typeof badges = badges;

  // @ts-ignore
  @Input() load: boolean;

  // @ts-ignore
  @Input() data: Meeting;

  showDetails: boolean = false;

  edit: boolean = false;

  // @ts-ignore
  tempData: Meeting;

  message: string = '';

  simpleload: boolean = false;


  started_at: string = '';
  ends_at: string = '';
  day: string = '';

  updated: boolean = false;

  @Output() remove = new EventEmitter<number>();

  constructor(private _meetingService: MeetingService, private  _authService: AuthService) {  }

  removeMeeting(){
    this.remove.emit(this.data.id);
  }

  handleChooseBadge(value: string){
    this.tempData.badge = value;
  }

  isNotPassed(){
    return (new Date(this.data.startedAt)) > (new Date());
  }

  ngOnInit(): void {
    this.tempData = {...this.data};
  }

  handleToggleRetro(value: boolean){
    this.tempData.retro = value;
  }

  showModal(){
    this.showDetails = !this.showDetails;
    if(!this.showDetails){
      this.edit = false;
    }
  }

  parseDate(date: string){
    return moment(date).calendar();
  }

  addZero(i: any) {
    if (i < 10) {i = "0" + i}
    return i;
  }

  handleEditEvent(){
    if([this.tempData.title, this.tempData.comment, this.started_at, this.ends_at, this.day].every(x => x)){
      this.simpleload = true;
      let startedDate = new Date(`${this.day} ${this.started_at}`);
      let endsDate = new Date(`${this.day} ${this.ends_at}`);

      if(endsDate > startedDate){
        let data: any = {
          badge: this.tempData.badge,
          title: this.tempData.title,
          comment: this.tempData.comment,
          retro: this.tempData.retro,
          startedAt: startedDate,
          endsAt: endsDate
        };

        this._meetingService.updateMeeting(data, this.tempData.id).subscribe((data: any) => {
            if(data.id){
              this.simpleload = false;
              this.updated = true;
              this.edit = false;
              this.data = {...data};
              this.tempData = {...data};
              setTimeout(() => {
                this.updated = false;
              }, 5000);
            }else{
              this.simpleload = false;
              this.message = 'Une erreur s\'est produite, essayez à nouveau';
            }
          },
          error => {
            this.simpleload = false;
            if(error.error['hydra:description'] === "La date choisie correspond à une réunion"){
              this.message = 'La date choisie correspond à une réunion';
            }else{
              this.message = 'Une erreur s\'est produite, veillez bien remplir le formulaire';
            }
          }
        );
      }else{
        this.simpleload = false;
        this.message = 'Veillez bien remplir le formulaire';
      }
    }else{
      this.simpleload = false;
      this.message = 'Veillez bien remplir le formulaire';
    }
  }

  requestEdition(){
    let startDate = new Date(this.tempData.startedAt);
    let endsDate = new Date(this.tempData.endsAt);

    this.started_at = `${this.addZero(startDate.getHours())}:${this.addZero(startDate.getMinutes())}`;
    this.ends_at = `${this.addZero(endsDate.getHours())}:${this.addZero(endsDate.getMinutes())}`;

    this.day = (new Date(this.tempData.startedAt)).toISOString().slice(0,10);

    this.edit = true;
  }
}
