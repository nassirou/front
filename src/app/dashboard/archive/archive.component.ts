import {Component, OnInit, SimpleChanges} from '@angular/core';
import {MeetingService} from "../../../service/meeting.service";

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.css']
})
export class ArchiveComponent implements OnInit {

  activatedView: string = 'all';
  showFilter: boolean = false;

  load: boolean = true;
  listing: Array<any> = [];
  page: number = 1;
  filter: string = '';
  // @ts-ignore
  retro: boolean | null;
  // @ts-ignore
  day: Date;
  totalMeetings: number = 0;

  constructor(private _meetingService: MeetingService) { }

  ngDoCheck() {

  }

  allMeetings(){
    this.toggleActivatedView('all');
    this.listing = [];
    this.page = 1;
    this.findData();
  }

  toggleRetro($value: boolean){
    if($value === this.retro){
      this.retro = null;
    }else{
      this.retro = $value;
    }
    this.listing = [];
    this.page = 1;
    this.findData();
  }

  findData(){
    this.load = true;
    this._meetingService.getPassedMeetings({
      page: this.page,
      filter: this.filter,
      retro: this.retro,
      day: this.day
    }).subscribe((data: any) => {
        if(this.page <= 1){
          this.totalMeetings = data['hydra:totalItems'];
          this.listing = data['hydra:member'];
        }else{
          console.log([...this.listing, ...data['hydra:member']]);
          this.listing = [...this.listing, ...data['hydra:member']];
        }
        this.load = false;
      },
      error => {
        this.load = false;
        console.log(error);
      });
  }

  nextPage(){
    this.page++;
    this.findData();
  }

  ngOnInit(): void {
    this.findData();
  }

  ngOnChanges(changes: SimpleChanges) {

  }

  toggleActivatedView($value: string){
    this.activatedView = $value;
  }

  toggleShowFilter(){
    this.showFilter = !this.showFilter;
  }
}
