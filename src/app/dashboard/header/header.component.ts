import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Output() toggleSideBar = new EventEmitter<boolean>();

  constructor(private _router: Router) { }

  ngOnInit(): void {
  }

  logout(){
    localStorage.removeItem('personnal');
    localStorage.removeItem('token');
    localStorage.removeItem('refresh_token');

    this._router.navigate(['/']);
  }

  handleClickSideBar(){
    this.toggleSideBar.emit();
  }

}
