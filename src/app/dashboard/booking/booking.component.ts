import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {MeetingService} from "../../../service/meeting.service";
import {Meeting} from "../../models/meeting";

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit, OnChanges {

  activatedView: string = 'all';
  showFilter: boolean = false;

  load: boolean = true;
  listing: Array<any> = [];
  page: number = 1;
  filter: string = '';
  // @ts-ignore
  retro: boolean | null;
  // @ts-ignore
  day: Date;
  totalMeetings: number = 0;

  constructor(private _meetingService: MeetingService) { }

  deleteMeeting($event: any){
    this._meetingService.deleteMeeting($event).subscribe(
      (data: any) => {
        this.listing = this.listing.filter((x: Meeting) => x.id != $event);
        this.totalMeetings = this.totalMeetings - 1;
      },
      error => {

      }
    )
  }

  allMeetings(){
    this.toggleActivatedView('all');
    this.listing = [];
    this.page = 1;
    this.findData();
  }

  toggleRetro($value: boolean){
    if($value === this.retro){
      this.retro = null;
    }else{
      this.retro = $value;
    }
    this.listing = [];
    this.page = 1;
    this.findData();
  }

  findData(){
    this.load = true;
    this._meetingService.getMeetings({
      page: this.page,
      filter: this.filter,
      retro: this.retro,
      day: this.day
    }).subscribe((data: any) => {
        if(this.page <= 1){
          this.totalMeetings = data['hydra:totalItems'];
          this.listing = data['hydra:member'];
        }else{
          this.listing = [...this.listing, ...data['hydra:member']];
        }
        this.load = false;
      },
      error => {
        this.load = false;
        console.log(error);
      });
  }

  nextPage(){
    this.page++;
    this.findData();
  }

  ngOnInit(): void {
    this.findData();
  }

  ngOnChanges(changes: SimpleChanges) {

  }

  toggleActivatedView($value: string){
    this.activatedView = $value;
  }
}
