import {Component, Input, OnInit} from '@angular/core';
import {MeetingService} from "../../../service/meeting.service";
import {Meeting} from "../../models/meeting";
import * as moment from "moment"
import 'moment/locale/fr';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Input() user: any = {}

  upcomming: Array<Meeting> = [];

  loader: boolean = true;

  // @ts-ignore
  @Input() displayMe: boolean;

  constructor(private _meetingService: MeetingService) { }

  ngOnInit(): void {
    this._meetingService.getUpcomming().subscribe((data: any) => {
      this.upcomming = data['hydra:member'].splice(0, 10);
      this.loader = false;
    });
  }

  parseDate(date: string){
    return moment(date).calendar();
  }

  handleCloseSideBar(){
    this.displayMe = false;
  }

}
