export const dayLabels: Array<string> = [
  'Dim',
  'Lun',
  'Mar',
  'Mer',
  'Jeu',
  'Ven',
  'Sam'
];

export const monthLabel: Array<string> = [
  'Janvier',
  'Février',
  'Mars',
  'Avril',
  'Mai',
  'Juin',
  'Juillet',
  'Aout',
  'Septembre',
  'Octobre',
  'Novembre',
  'Décembre'
];

export const badges : Array<any> = [
  {
    title: 'bolt',
    value: 'Urgence'
  },
  {
    title: 'trending_up',
    value: 'Evolution des activités'
  },
  {
    title: 'group_work',
    value: 'Travail de groupe'
  },
  {
    title: 'work',
    value: 'Business'
  },
  {
    title: 'explore',
    value: 'Marketing'
  },
  {
    title: 'announcement',
    value: 'Annonce'
  },
  {
    title: 'rocket_launch',
    value: 'Décolage'
  },
  {
    title: 'trending_down',
    value: 'Crise'
  }
];

export const samplecolor: Array<string> = [
  '#e63946',
  '#bb3e03',
  '#560bad',
  '#90be6d',
  '#006d77',
  '#22223b',
  '#5a189a',
  '#09bc8a',
  '#ffd23f'
];

export const acceptedHours: Array<number> = [
  7,8,9,10,11,12,13,14,15,16,17,18,19
];
