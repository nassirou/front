import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  showPassword: boolean = false;
  username: string = '';
  password: string = '';
  message: string = '';
  loading: boolean = false;

  constructor(private authService: AuthService, private _router: Router) { }

  ngOnInit(): void {}

  handleChangeViewPassword(){
    this.showPassword = !this.showPassword;
  }

  login(){
    this.loading = true;
    this.authService.login({
      username: this.username,
      password: this.password
    }).subscribe((data: any) => {
      // @ts-ignore
      if(data.success){
        // @ts-ignore
        localStorage.setItem('personnal', JSON.stringify(data.user));
        localStorage.setItem('refresh_token', data.token_refresh);
        // @ts-ignore
        localStorage.setItem('token', data.token);
        this._router.navigate(['/dashboard']);
      }else{
        this.loading = false;
        // @ts-ignore
        this.message = data.message;
      }
    }, (error) => {
      console.log(error);
      if(error.status === 401){
        this.loading = false;
        this.message = 'Nom d\'utilisateur ou mot de passe incorrect';
      }else{
        this.loading = false;
        this.message = 'Une erreur s\'est produite, essayez à nouveau';
      }
    })
  }
}
