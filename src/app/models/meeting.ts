export interface Meeting {
  id: number;
  title: string;
  comment: string;
  retro: boolean;
  startedAt: string;
  endsAt: string;
  user: any;
  createdAt: string;
  badge?: string;
  isAborted?: boolean;
  updatedAt: string;
  updatedUser?: any;
  isEditable?: boolean;
}
