import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import {AuthGuard} from "./auth.guard";
import {CalendarComponent} from "./dashboard/calendar/calendar.component";
import {ResumeComponent} from "./dashboard/resume/resume.component";
import {BookingComponent} from "./dashboard/booking/booking.component";
import {ArchiveComponent} from "./dashboard/archive/archive.component";

const routes: Routes = [
  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [AuthGuard],
    data: {
      title: "Tableau de bord"
    },
    children: [
      {
        path: '',
        component: ResumeComponent
      },
      {
        path: 'calendar',
        component: CalendarComponent
      },
      {
        path: 'booking',
        component: BookingComponent
      },
      {
        path: 'archive',
        component: ArchiveComponent
      }
    ]
  },
  {
    path: "login",
    component: LoginComponent,
    data: {
      title: "Accéder à mon compte"
    }
  },
  {
    path: "",
    component: LoginComponent,
    data: {
      title: "Accéder à mon compte"
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
